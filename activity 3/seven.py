
class Student:
    def __init__(self,first_name, middle_name, last_name):
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name

    def get_full_name(self):
        return self.first_name+" "+ self.middle_name+" "+ self.last_name

name = Student("John", "Ivan", "Flores")



print(name.get_full_name())