import datetime


def same_type(val1,val2):
    if type(val1) == type(val2):
        if isinstance(val1, str):
            return val1+" "+val2
        elif isinstance(val1, int):
            return val1 * val2
        elif isinstance(val1, list):
            return val1 + val2
        elif isinstance(val1, dict):
            dictionary={**val1, **val2}
            return dictionary
        elif isinstance(val1, datetime.date):
            date = val2 - val1
            return date

    else:
        print("Not the same type!")


#string
print(same_type("one", "piece"))



#int
print(same_type(3, 1))
#list

list1 = ["2","2","2"]
list2 = [2,2,3,4]
print(same_type(list1, list2))
#dictionary
months1= {
	"Jan": "January",
	"Feb": "Febuary",
	"Mar": "March"

}

months2= {
	"Sep": "September",
	"OCt": "October",
	"Dec": "December"

}
print(same_type(months1, months2))
#datetime object
date1 = datetime.datetime(2020, 5, 16)
date2 = datetime.datetime(2020, 5, 17)

print(same_type(date1, date2))