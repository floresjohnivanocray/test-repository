def number_of(n):
    return ((abs(1000 - n) <= 100) or (abs(2000 - n) <= 100))


print(number_of(900))
print(number_of(1900))
