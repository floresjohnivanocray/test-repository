class Animal:
    def __init__(self,eyes,legs):
        self.eyes = eyes
        self.legs = legs


class Cat(Animal):
    def __init__(self,eyes,legs,tail):
        super().__init__(eyes,legs)
        self.tail = tail
    def body_part(self):
        return "Numeber of eyes: "+str(self.eyes)+"\nNumber of legs: "+ str(self.legs)+"\nNumber of tail: " + str(self.tail)



animal = Cat(2,4,1)
print(animal.body_part())