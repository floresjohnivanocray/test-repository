def oddOrEven(num):
    return 'even' if num % 2 == 0 else 'odd'

num = int(input("Enter a number: "))

print(oddOrEven(num))