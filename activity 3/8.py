from seven import Student
class Teacher(Student):

    def get_full_name(self):

        fullname= self.first_name+" "+ self.middle_name+" "+ self.last_name
        return fullname.upper()


name = Teacher("John", "Ivan", "Flores")

print(name.get_full_name())